from .company_views import CompanyDetail, CompanyList
from .gym_views import GymDetail, GymList
from .home_view import HomeView
from .gymclasses_views import GymClassesDetail, GymClassesList
from .gymactivities_views import GymActivitiesDetail, GymActivitiesList



