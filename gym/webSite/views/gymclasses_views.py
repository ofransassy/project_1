from django.views.generic import  DetailView, ListView, View
from django.shortcuts import get_object_or_404, render, redirect
from ..models import GymClasses
from django.db.models import Count

class GymClassesDetail(DetailView):
    model = GymClasses
    template_name = 'webSite/gymclasses_detail.html'

class GymClassesList(ListView):
    model = GymClasses
    context_object_name = "gymclasses"
    Comp_Devices = GymClasses.objects.annotate(no_of_classes=Count('class_name'))
    template_name = 'webSite/gymclasses_list.html'