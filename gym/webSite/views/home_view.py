from django.views.generic import View
from django.shortcuts import get_object_or_404, render, redirect
from ..models import Company



class HomeView(View):
    template_name = 'webSite/home.html'

    def get(self, request, *args, **kwargs):
        name_company = Company.objects.order_by('name_company')
        context= dict(latest_user=name_company)

        return render(request, self.template_name, context)

