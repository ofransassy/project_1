from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView, View
from django.shortcuts import get_object_or_404, render, redirect
from ..models import Company


class CompanyDetail(DetailView):
    model = Company
    template_name = 'webSite/company_detail.html'


class CompanyList(ListView):
    model = Company
    context_object_name = "companies"
    template_name = 'webSite/company_list.html'
