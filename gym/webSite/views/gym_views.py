from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView, View
from django.shortcuts import get_object_or_404, render, redirect
from ..models import Gym

class GymDetail(DetailView):
    model = Gym
    template_name = 'webSite/gym_detail.html'

class GymList(ListView):
    model = Gym
    context_object_name = "gyms"
    template_name = 'webSite/gym_list.html'