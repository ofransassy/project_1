from django.views.generic import  DetailView, ListView, View
from django.shortcuts import get_object_or_404, render, redirect
from ..models import GymActivities

class GymActivitiesDetail(DetailView):
    model = GymActivities
    template_name = 'webSite/gymactivities_detail.html'

class GymActivitiesList(ListView):
    model = GymActivities
    context_object_name = "gymactivities"
    template_name = 'webSite/gymactivities_list.html'