from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView, View
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.utils import timezone
from .models import UserProfile, Company, Gym, GymClasses, GymActivities, Schedule
from .forms import EditProfileForm, RegistrationForm, MemberForm
from django.contrib.auth import authenticate
from .import forms
from django.contrib.auth.decorators import login_required



def register(request):
    if request.method =='POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('registration:home'))
    else:
        form = RegistrationForm()

        args = {'form': form}
        return render(request, 'registration/reg_form.html', args)

def addmem(request):
    if request.method =='POST':
        form = MemberForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('registration:home'))
    else:
        form = MemberForm()
        args = {'form': form}
        return render(request, 'registration/addmem.html', args)



class UserProfileList(ListView):

    model = UserProfile
    template_name = 'userProfile/profile_list.html'

class UserProfileDetail(DetailView):
    model = UserProfile
    context_object_name = 'profile'
    template_name = 'userProfile/profile_details.html'

class UserProfileCreate(CreateView):
    model = UserProfile
    template_name = 'userProfile/profile_form.html'
    form_class = RegistrationForm

class ProfileUpdateView(UpdateView):
    model = UserProfile
    template_name = 'userProfile/edit_profile.html'
    form_class = EditProfileForm

    def get_success_url(self):
        url = "webSite:profile_details"
        return reverse(url, kwargs={'pk': self.request.user.id})





def edit_profile(request):
    submitted = False
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            cd = form.cleaned_data
            # form.save()
            # args = {'form': form}
            return render(request, 'userProfile/profile_details.html', args)
    else:
        form = EditProfileForm(instance=request.user)
        args = {'form': form}
        if 'submitted' in request.GET:
            submitted = True
        return render(request, 'userProfile/edit_profile.html', {'form': form, 'submitted': submitted})

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect(reverse('userProfile:profile_details'))
        else:
            return redirect(reverse('registration:change_password'))
    else:
        form = PasswordChangeForm(user=request.user)

        args = {'form': form}
        return render(request, 'registration/change_password.html', args)

# *****************************************************************************************************************
class IndexView(View):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        name_company = Company.objects.order_by('name_company')
        context= dict(latest_user=name_company)
        print(request.user.is_authenticated)
        return render(request, self.template_name, context)
    # def get_queryset(self):
    #     return Company.objects.filter(pub_date__lte=timezone.now()).order_by('address_company')[:5]


# Company CRUD
class CompanyList(ListView):
    model = Company
    context_object_name = "companies"
    template_name = 'company/company_list.html'


    # def get_queryset(self):
    #     query = super(CompanyList, self).get_query_set
    #     return query

class ScheduleCreate(CreateView):
    model = Schedule
    fields = '__all__'
    template_name = 'admin/schedule/change_list.html'


class CompanyDetail(DetailView):
    model = Company
    template_name = 'company/company_details.html'

class CompanyCreate(CreateView):
    model = Company
    template_name = 'company/company_details.html'

class CompanyUpdate(UpdateView):
    model = Company
    template_name = 'company/company_list.html'

class CompanyDelete(DeleteView):
    model = Company
    # template_name = 'company/form.html'

def company_create(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect(reverse('company:company_create'))
    else:
        form = CreateCompany(instance=request.user)
        args = {'form': form}
        return render(request, 'company/company_create.html', args)




# UserProfile CRUD.
# ************************************************************************************************************

# Gym CRUD.
# ************************************************************************************************************
class GymList(ListView):

    model = Gym
    template_name = 'gym/gym_list.html'

class GymDetail(DetailView):
    model = Gym
    template_name = 'gym/gym_details.html'

class GymCreate(CreateView):
    model = Gym
    template_name = 'gym/gym_details.html'

class GymUpdate(UpdateView):
    model = Gym
    template_name = 'gym/gym_list.html'

class GymDelete(DeleteView):
    model = Gym
#gymClasses CRUD and finctions
# ****************************************************************************************************************

class ActivityDetail(DetailView):  # roger
    model = GymClasses
    template_name = 'activities/detail.html'

class ResultsView(DetailView):  # roger
    model = GymClasses
    template_name = 'activities/result.html'

def detail(request, gymclasses_id):  #roger
    return HttpResponse("You are looking at gym class %s." % gymclasses_id)

def detail(request, gymclasses_id): #roger
    gymclasses = get_object_or_404(GymClasses, pk=gymclasses_id)
    return render(request, 'activities/detail.html', {'gymclasses': gymclasses})

# def result(request, gymclasses_id): #roger
#     response = "You're looking at the choices you've made from the gym class %s."
#     return HttpResponse(response % gymclasses_id)

def choices(request, gymclasses_id): #roger
    return HttpResponse("You're choosing from this gym class %s." % gymclasses_id)

def choices(request, gymclasses_id): #roger
    gymclasses = get_object_or_404(GymClasses, pk=gymclasses_id)
    try:
        selected_choice = gymclasses.choice_set.get(pk=request.POST['choice'])
    except (KeyError, GymActivities.DoesNotExist):
        # Redisplay the gym class you're choosing form.
        return render(request, 'activities/detail.html', {
            'gymclass': gymclass,
            'error_message': "You didn't select an activity.",
        })
    else:
        selected_choice.choices += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('ActivityDetail:result', args=(gymclass.id,)))
# *********************************************************************************************************************
