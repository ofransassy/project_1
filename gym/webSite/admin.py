from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin
from .utils import EventCalendar
from django.contrib.auth.models import Group
admin.site.site_header= 'Gym Admin Dashboard'
import datetime
import calendar
from django.urls import reverse
from calendar import HTMLCalendar
from django.utils.safestring import mark_safe
from .models import UserProfile, Company, Gym, GymClasses, GymActivities, Schedule, Profile

#

class GymAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'company', 'email')

class GymClassesAdmin(admin.ModelAdmin):
    list_display = ('class_name','class_capacity', 'class_participants', 'active')

class GymActivitiesAdmin(admin.ModelAdmin):
    list_display = ('class_name','activity_name', 'instructor', 'active')


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'created_at')
    list_filter = ('created_at', 'updated_at',)

class ScheduleAdmin(admin.ModelAdmin):
    list_display = ['day', 'start_time', 'end_time', 'notes']
    change_list_template = 'admin/schedule/change_list.html'

    def changelist_view(self, request, extra_context=None):
        after_day = request.GET.get('day__gte', None)
        extra_context = extra_context or {}

        if not after_day:
            d = datetime.date.today()
        else:
            try:
                split_after_day = after_day.split('-')
                d = datetime.date(year=int(split_after_day[0]), month=int(split_after_day[1]), day=1)
            except:
                d = datetime.date.today()

        previous_month = datetime.date(year=d.year, month=d.month, day=1)  # find first day of current month
        previous_month = previous_month - datetime.timedelta(days=1)  # backs up a single day
        previous_month = datetime.date(year=previous_month.year, month=previous_month.month,
                                       day=1)  # find first day of previous month

        last_day = calendar.monthrange(d.year, d.month)
        next_month = datetime.date(year=d.year, month=d.month, day=last_day[1])  # find last day of current month
        next_month = next_month + datetime.timedelta(days=1)  # forward a single day
        next_month = datetime.date(year=next_month.year, month=next_month.month,
                                   day=1)  # find first day of next month

        extra_context['previous_month'] = reverse('webSite:change_list') + '?day__gte=' + str(
            previous_month)
        extra_context['next_month'] = reverse('webSite:change_list') + '?day__gte=' + str(next_month)

        cal = EventCalendar()
        html_calendar = cal.formatmonth(d.year, d.month, withyear=True)
        html_calendar = html_calendar.replace('<td ', '<td  width="150" height="150"')
        extra_context['calendar'] = mark_safe(html_calendar)
        return super(ScheduleAdmin, self).changelist_view(request, extra_context)


admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(Profile, GroupAdmin)
admin.site.register(UserProfile)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Gym, GymAdmin)
admin.site.unregister(Group)
admin.site.register(GymClasses, GymClassesAdmin)
admin.site.register(GymActivities, GymActivitiesAdmin)



#
# admin.site.site_url = "/"
# Register your models here.






