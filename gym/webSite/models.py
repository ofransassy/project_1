import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.forms import ModelForm
from django.urls import reverse
from django.utils import timezone
from phone_field import PhoneField



# get_user_model will return the currently active user model

class Profile(Group):
    class Meta:
        proxy = True
        app_label = 'auth'
        # verbose_name = _('Profile')
        verbose_name_plural = "Profiles"

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, default='DEFAULT VALUE')
    last_name = models.CharField(max_length=50, default='DEFAULT VALUE')
    email = models.EmailField(max_length=70, blank=True, null=True)
    address = models.CharField(max_length=80, default='')
    dob = models.DateField(null=True, blank=True)
    bio = models.TextField(blank=True, help_text="The bio entered by the user.")
    city = models.CharField(max_length=100, default='')
    phone = models.IntegerField(default=0)
    gender = models.CharField(max_length=1, blank=True, null=True, default=None)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)
# maybe fix those UserProfile signals


    def __str__(self):
        return f'{self.user.first_name} Profile'

    def create_profile(sender, **kwargs):
        if kwargs['created']:
            user_profile = UserProfile.objects.create(user=kwargs['instance'])


class Member(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, default=None)
    last_name = models.CharField(max_length=50, default=None)
    email = models.EmailField(unique=True, default=None)

    def __str__(self):
        return f'{self.user.first_name} Member'

    class Meta:
        abstract = True

# Now we are going to create class Company
class Company(models.Model):
    class Meta:
        verbose_name_plural = "companies"

    name = models.CharField(max_length=50)
    address = models.CharField(max_length=80)
    email = models.EmailField()
    phone = PhoneField(blank=True, help_text='Contact phone number')
    bio = models.TextField(blank=True, help_text="The bio entered by the company admin")
    owner = models.ForeignKey(User, on_delete=models.CASCADE, default="", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % self.name


class UserProfileManager(models.Manager):
    # a Company has only one Manager
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                )
    company = models.OneToOneField(Company, on_delete=models.CASCADE, primary_key=True, )

    def __str__(self):
        return "%s Profiles" % self.user.name

    def active(self):
        return super().get_queryset().filter(active=True)

    def expired(self):
        return super().get_queryset().filter(active=False)

    # decorator
    # we use unicode here in case the name is not in ASCII (Arabic, Chinese etc..)
    # def __unicode__(self):
    #     return f'{self.get_full_name()} Profiles'


class Gym(models.Model):
    name = models.CharField(max_length=50, default=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # one Company has many Gyms
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    location = models.CharField(max_length=80)
    email = models.EmailField(unique=True, default='DEFAULT VALUE')
    bio = models.TextField(blank=True, help_text="The bio entered by gym admin.")
    phone = PhoneField(blank=True, help_text='Contact phone number')


    def __str__(self):
        return '%s' % self.name


class GymClasses(models.Model):
    class Meta:
        verbose_name_plural = "gym classes"

    class_name = models.CharField(max_length=255)
    class_capacity = models.IntegerField(default=0)
    class_participants = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    live_date = models.DateTimeField()
    expiry_date = models.DateTimeField()
    active = models.BooleanField()
    belonging_gym = models.ManyToManyField(Gym)


    def clean(self):
        num_capacity = self.class_capacity
        num_participants = self.class_participants
        if num_participants >num_capacity :
            raise ValidationError ('Class Participants are Surpassing the Class Capacity')

    def __str__(self):
        return self.class_name

    def __unicode__(self):
        return self.class_name

    objects = models.Manager()
    UserProfileManager = UserProfileManager()


class GymActivities(models.Model):
    class Meta:
        verbose_name_plural = "Gym Activities"

    class_name = models.ForeignKey(GymClasses, on_delete=models.CASCADE, default="", null=True, blank=True)  # roger
    activity_name = models.CharField(max_length=200, default=None, null=True)
    # choices = models.IntegerField(default=0)  # choices=votes
    schedule_day = models.DateField()
    schedule_time = models.TimeField()
    max_attend = models.IntegerField()
    active = models.BooleanField()
    instructor = models.ForeignKey(User, on_delete=models.CASCADE, default="", null=True, blank=True)


    def clean(self):
        # print(self.instructor.is_active)
        if not self.instructor.is_active :
            raise ValidationError('This Trainer is unavailable at the moment, please select another')


    def __str__(self):
        return self.activity_name


class Schedule(models.Model):
    class Meta:
        verbose_name_plural = "Scheduling"

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    activity_name = models.ForeignKey(GymActivities, on_delete=models.CASCADE)
    day = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    notes = models.TextField(help_text="Textual Notes", blank=True, null=True)



    def check_overlap(self, fixed_start, fixed_end, new_start, new_end):
        overlap = False
        if new_start == fixed_end or new_end == fixed_start:  # edge case
            overlap = False
        elif (new_start >= fixed_start and new_start <= fixed_end) or (
            new_end >= fixed_start and new_end <= fixed_end):  # innner limits
            overlap = True
        elif new_start <= fixed_start and new_end >= fixed_end:  # outter limits
            overlap = True
        return overlap

    def get_absolute_url(self):
        url = reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name), args=[self.id])
        return u'<a href="%s">%s</a>' % (url, str(self.start_time))


    def clean(self):
        if self.end_time <= self.start_time:
            raise ValidationError('Ending times must be after the starting times')

        events = Schedule.objects.filter(day=self.day)
        if events.exists():
            for event in events:
                if self.check_overlap(event.start_time, event.end_time, self.start_time, self.end_time):
                    raise ValidationError(
                        'There is an overlap with another event: ' + str(event.day) + ', ' + str(
                            event.start_time) + '-' + str(event.end_time))

