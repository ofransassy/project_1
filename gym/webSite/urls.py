from django.urls import path

from . import views

app_name = 'webSite'
urlpatterns = [
    # path('', views.IndexView.as_view(), name='index'), #fix this
    # path('<int:pk>/', views.ManagerDetailView.as_view(), name='detail'), #fix this too

#     path('register/', views.register, name='register'),
#     # path('edit_profile/', views.edit_profile, name='edit_profile'),
#     path('schedule/', views.ScheduleCreate.as_view(template_name='change_list.html'), name='change_list'),
#
#     path('company_create/', views.company_create, name='company_create'),
     path('', views.HomeView.as_view(), name='home'),
     path('companies', views.CompanyList.as_view(), name='company_list'),
     path('companies/<int:pk>', views.CompanyDetail.as_view(), name='company_detail'),



     path('gyms', views.GymList.as_view(), name='gym_list'),
     path('gyms/<int:pk>', views.GymDetail.as_view(), name='gym_detail'),
     path('gymclasses/<int:pk>', views.GymClassesDetail.as_view(), name='gymclasses_detail'),
     path('gymclasses', views.GymClassesList.as_view(), name='gymclasses_list'),
     path('gymactivities/<int:pk>', views.GymActivitiesDetail.as_view(), name='gymactivities_detail'),
     path('gymactivities', views.GymActivitiesList.as_view(), name='gymactivities_list'),


     #     path('create', views.CompanyCreate.as_view(), name='company_create'),
#     path('update/<int:pk>', views.CompanyUpdate.as_view(), name='company_update'),
#     path('delete/<int:pk>', views.CompanyDelete.as_view(), name='company_delete'),
#
# #     userProfile Paths
#     path('userProfiles', views.UserProfileList.as_view(), name='profile_list'),
#     path('userProfile/<int:pk>', views.UserProfileDetail.as_view(), name='profile_details'),
#     path('userProfile/<int:pk>/edit', views.ProfileUpdateView.as_view(), name='edit_profile'),
#     path('create', views.UserProfileCreate.as_view(), name='company_create'),
#     path('addmem/', views.addmem, name='addmem'),
# #     userProfile Paths
#     path('gyms', views.GymList.as_view(), name='gym_list'),
#     path('gym/<int:pk>', views.GymDetail.as_view(), name='gym_details'),
#     path('create', views.GymCreate.as_view(), name='gym_details'),
#     path('update/<int:pk>', views.GymUpdate.as_view(), name='gym_list'),
#     path('delete/<int:pk>', views.GymDelete.as_view(), name='company_delete'),
#
# #     gymClasses Paths
#     path('<int:pk>/', views.ActivityDetail.as_view(), name='detail'),
#     path('<int:pk>/results/', views.ResultsView.as_view(), name='result'),
#     path('changePassword', views.ProfileUpdateView.as_view(), name='change_password'),
]
