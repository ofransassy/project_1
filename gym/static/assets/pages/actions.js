 var initActionsList = function () {
        var url = $('#dataTable').data('urlajax');
        var th_styling = $("#dataTable").find("th");
        //  get the styling of rows
        var row_class = {targets: [], className: 'text-center'}
        $(th_styling).each(function( index ) {
            let class_name = $(this).data('classname')
            if (row_class.className.includes(class_name)){
                 row_class.targets.push(index);
            }
        });

        var dt_table = $('#dataTable').dataTable({
            order: [[ 0, "desc" ]],
            columnDefs: [
               row_class,
            ],
            searching: true,
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: url,
        });
        // default config
         var default_config = {
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true,
                closeOnCancel: false,
                confirmButtonColor: '#f60e0e',
            };
         var delete_form = $("#deleteForm")

        $( document ).on("click", ".delete_prm", function(event){
            event.preventDefault();
            let custom_config = this.dataset
            default_config =  $.extend(default_config, custom_config)
            let action_form = custom_config['actionform']
            swal(default_config, function(isConfirm){
                if (isConfirm) {
                    $(delete_form).attr('action',action_form )
                    $(delete_form).submit();
                }else{
                    let sa_popupTitleCancel =  custom_config['popuptitlecancel']
                    swal(sa_popupTitleCancel, '', "error");
                }
            });

        });

        var restore_form = $("#restoreForm")

        $( document ).on("click", ".restore_prm", function(event){
            event.preventDefault();
            let custom_config = this.dataset
            default_config =  $.extend(default_config, custom_config)
            let action_form = custom_config['actionform']
            swal(default_config, function(isConfirm){
                if (isConfirm) {
                    $(restore_form).attr('action', action_form )
                    $(restore_form).submit();
                }else{
                    let sa_popupTitleCancel =  custom_config['popuptitlecancel']
                    swal(sa_popupTitleCancel, '', "error");
                }
            });
        });
    }

 $(document).ready(function() {
    initActionsList();
 })
