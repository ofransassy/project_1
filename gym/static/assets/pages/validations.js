var FormValidationMd = function() {
    var unique_validation = function(){
    // TODO: to improve
    var token = $("input[name='csrfmiddlewaretoken']").val();

    $(".unique-validation").change(function () {
      var field = $(this)
      var span_message = $(field).parents('.form-group').children('.help-block')
      var data_field = this.dataset
      var field_value = $(this).val();
      var field_name = data_field['fieldname'];
      var model_name = data_field['modelname'];
      var url_validation = $(this).closest("form").attr("data-validate-username-url")
      if(field_value && field_name  && model_name && url_validation){
          var json_data = {'model_name': model_name, 'field_name': field_name, 'field_value': field_value }
          $.ajax({
            type: "POST",
            headers: { "X-CSRFToken": token },
            url: url_validation,
            data: json_data,
            dataType: 'json',
            success: function (data) {
              if (data.is_taken) {
                $(field).parents('.form-group').addClass('has-error');
                $(span_message).text(data.error_message);
              }
              else{
                $(field).parents('.form-group').removeClass('has-error');
                $(span_message).text(data.success_message);
              }
            },
            error : function(error) {
                console.log(error)
            }
          });
       }

    });
    }
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation
        var form1 = $('#form_sample_1');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input

            rules: {
                name: {
                    minlength: 2,
                    maxlength: 100,
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                creation_date: {
                    required: true,
                    email: true
                },
                siren: {
                    required: true,
                    minlength: 9,
                    maxlength: 9,
                },
                representative_fname: {
                    required: true,
                },
                representative_lname: {
                    required: true,
                },
                zip_code: {
                    required: true,
                    number: true
                },
                city: {
                    required: true,
                },
                street: {
                    required: true,
                },

            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function(form) {
                success1.show();
                error1.hide();
            }
        });
    }
    return {
        //main function to initiate the module
        init: function() {
            handleValidation1();
            unique_validation();
        }
    };
}();

jQuery(document).ready(function() {
    FormValidationMd.init();
});